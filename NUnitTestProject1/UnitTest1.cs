using NUnit.Framework;

namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void HakenReinUndRaus()
        {
            //Arrange
            Kran kran = new Kran;
            Panel haken = new Panel;
            haken.Size = new Size();
            haken.Location = new Point();

            Panel basis = new Panel();
            basis.Location = Point();

            //Act
            kran.HakenRaus(haken, basis);
            
            //Assert
            Assert.AreNotEqual(haken.Height + haken.Location.Y, basis.Location.Y);
        }
    }
}