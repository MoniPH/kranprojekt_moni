﻿namespace KranProjekt_Moni
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelAusleger = new System.Windows.Forms.Panel();
            this.buttonMoveRight = new System.Windows.Forms.Button();
            this.buttonMoveLeft = new System.Windows.Forms.Button();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.panelMast = new System.Windows.Forms.Panel();
            this.panelBasis = new System.Windows.Forms.Panel();
            this.panelHaken = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.buttonHakenRein = new System.Windows.Forms.Button();
            this.buttonHakenRaus = new System.Windows.Forms.Button();
            this.buttonHakenRechts = new System.Windows.Forms.Button();
            this.buttonHakenLinks = new System.Windows.Forms.Button();
            this.panelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelAusleger
            // 
            this.panelAusleger.BackColor = System.Drawing.Color.Blue;
            this.panelAusleger.Location = new System.Drawing.Point(45, 48);
            this.panelAusleger.Name = "panelAusleger";
            this.panelAusleger.Size = new System.Drawing.Size(259, 40);
            this.panelAusleger.TabIndex = 0;
            this.panelAusleger.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // buttonMoveRight
            // 
            this.buttonMoveRight.Location = new System.Drawing.Point(14, 31);
            this.buttonMoveRight.Name = "buttonMoveRight";
            this.buttonMoveRight.Size = new System.Drawing.Size(102, 29);
            this.buttonMoveRight.TabIndex = 1;
            this.buttonMoveRight.Text = "Move Right";
            this.buttonMoveRight.UseVisualStyleBackColor = true;
            this.buttonMoveRight.Click += new System.EventHandler(this.buttonMoveRight_Click);
            // 
            // buttonMoveLeft
            // 
            this.buttonMoveLeft.Location = new System.Drawing.Point(14, 77);
            this.buttonMoveLeft.Name = "buttonMoveLeft";
            this.buttonMoveLeft.Size = new System.Drawing.Size(102, 29);
            this.buttonMoveLeft.TabIndex = 2;
            this.buttonMoveLeft.Text = "Move Left";
            this.buttonMoveLeft.UseVisualStyleBackColor = true;
            this.buttonMoveLeft.Click += new System.EventHandler(this.buttonMoveLeft_Click);
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Location = new System.Drawing.Point(14, 176);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(102, 29);
            this.buttonMoveDown.TabIndex = 3;
            this.buttonMoveDown.Text = "Move Down";
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Location = new System.Drawing.Point(14, 125);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(102, 29);
            this.buttonMoveUp.TabIndex = 4;
            this.buttonMoveUp.Text = "Move Up";
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // panelMast
            // 
            this.panelMast.BackColor = System.Drawing.Color.Red;
            this.panelMast.Location = new System.Drawing.Point(254, 88);
            this.panelMast.Name = "panelMast";
            this.panelMast.Size = new System.Drawing.Size(50, 266);
            this.panelMast.TabIndex = 5;
            this.panelMast.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // panelBasis
            // 
            this.panelBasis.BackColor = System.Drawing.Color.Black;
            this.panelBasis.Location = new System.Drawing.Point(203, 354);
            this.panelBasis.Name = "panelBasis";
            this.panelBasis.Size = new System.Drawing.Size(128, 57);
            this.panelBasis.TabIndex = 6;
            // 
            // panelHaken
            // 
            this.panelHaken.BackColor = System.Drawing.Color.Green;
            this.panelHaken.Location = new System.Drawing.Point(45, 88);
            this.panelHaken.Name = "panelHaken";
            this.panelHaken.Size = new System.Drawing.Size(68, 65);
            this.panelHaken.TabIndex = 7;
            this.panelHaken.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // panelButtons
            // 
            this.panelButtons.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelButtons.Controls.Add(this.buttonHakenLinks);
            this.panelButtons.Controls.Add(this.buttonHakenRechts);
            this.panelButtons.Controls.Add(this.buttonHakenRein);
            this.panelButtons.Controls.Add(this.buttonHakenRaus);
            this.panelButtons.Controls.Add(this.buttonMoveRight);
            this.panelButtons.Controls.Add(this.buttonMoveLeft);
            this.panelButtons.Controls.Add(this.buttonMoveDown);
            this.panelButtons.Controls.Add(this.buttonMoveUp);
            this.panelButtons.Location = new System.Drawing.Point(669, 1);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(131, 468);
            this.panelButtons.TabIndex = 8;
            // 
            // buttonHakenRein
            // 
            this.buttonHakenRein.Location = new System.Drawing.Point(14, 268);
            this.buttonHakenRein.Name = "buttonHakenRein";
            this.buttonHakenRein.Size = new System.Drawing.Size(102, 29);
            this.buttonHakenRein.TabIndex = 6;
            this.buttonHakenRein.Text = "Haken Rein";
            this.buttonHakenRein.UseVisualStyleBackColor = true;
            this.buttonHakenRein.Click += new System.EventHandler(this.buttonHakenRein_Click);
            // 
            // buttonHakenRaus
            // 
            this.buttonHakenRaus.Location = new System.Drawing.Point(14, 223);
            this.buttonHakenRaus.Name = "buttonHakenRaus";
            this.buttonHakenRaus.Size = new System.Drawing.Size(102, 29);
            this.buttonHakenRaus.TabIndex = 5;
            this.buttonHakenRaus.Text = "Haken Raus";
            this.buttonHakenRaus.UseVisualStyleBackColor = true;
            this.buttonHakenRaus.Click += new System.EventHandler(this.buttonHakenRaus_Click);
            // 
            // buttonHakenRechts
            // 
            this.buttonHakenRechts.Location = new System.Drawing.Point(14, 312);
            this.buttonHakenRechts.Name = "buttonHakenRechts";
            this.buttonHakenRechts.Size = new System.Drawing.Size(102, 51);
            this.buttonHakenRechts.TabIndex = 7;
            this.buttonHakenRechts.Text = "Haken Rechts";
            this.buttonHakenRechts.UseVisualStyleBackColor = true;
            this.buttonHakenRechts.Click += new System.EventHandler(this.hakenRechts_Click);
            // 
            // buttonHakenLinks
            // 
            this.buttonHakenLinks.Location = new System.Drawing.Point(14, 377);
            this.buttonHakenLinks.Name = "buttonHakenLinks";
            this.buttonHakenLinks.Size = new System.Drawing.Size(102, 51);
            this.buttonHakenLinks.TabIndex = 8;
            this.buttonHakenLinks.Text = "Haken Links";
            this.buttonHakenLinks.UseVisualStyleBackColor = true;
            this.buttonHakenLinks.Click += new System.EventHandler(this.hakenLinks_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 461);
            this.Controls.Add(this.panelAusleger);
            this.Controls.Add(this.panelMast);
            this.Controls.Add(this.panelBasis);
            this.Controls.Add(this.panelHaken);
            this.Controls.Add(this.panelButtons);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAusleger;
        private System.Windows.Forms.Button buttonMoveRight;
        private System.Windows.Forms.Button buttonMoveLeft;
        private System.Windows.Forms.Button buttonMoveDown;
        private System.Windows.Forms.Button buttonMoveUp;
        private System.Windows.Forms.Panel panelMast;
        private System.Windows.Forms.Panel panelBasis;
        private System.Windows.Forms.Panel panelHaken;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Button buttonHakenRaus;
        private System.Windows.Forms.Button buttonHakenRein;
        private System.Windows.Forms.Button buttonHakenRechts;
        private System.Windows.Forms.Button buttonHakenLinks;
    }
}

