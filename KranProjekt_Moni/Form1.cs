﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KranProjekt_Moni
{
    public partial class Form1 : Form
    {
        int schrittweite = 10;

        public Form1()
        {
            InitializeComponent();

        }

        // Der Kran fährt nach rechts
        private void buttonMoveRight_Click(object sender, EventArgs e)
        {
            // ... bis zum Rand des rechten Fensters bzw. panelButton
            if (panelBasis.Right +schrittweite <= panelButtons.Left)
            //if (panelBasis.Location.X + panelButtons.Width <= panelButtons.Location.X)
            {
                // Alle Panels werden nach rechts verschoben
                panelHaken.Left += schrittweite;
                panelAusleger.Left += schrittweite;
                panelMast.Left += schrittweite;
                panelBasis.Left += schrittweite;
            }
        }

        // Der Kran fährt nach links ...
        private void buttonMoveLeft_Click(object sender, EventArgs e)
        {
            // ... bis zum Rand des linken Fensters
            if (panelAusleger.Left - schrittweite >= this.ClientRectangle.X)
            {
                // Alle Panels werden nach links verschoben
                panelHaken.Left -= schrittweite;
                panelAusleger.Left -= schrittweite;
                panelMast.Left -= schrittweite;
                panelBasis.Left -= schrittweite;
            }
        }

        // Kran bzw. Mast nach oben ausfahren ...
        private void buttonMoveUp_Click(object sender, EventArgs e)
        {
            // ... bis zum Rand des oberen Fensters
            if (panelAusleger.Top - schrittweite >= this.ClientRectangle.Y)
            {
                //Haken und Ausleger werden nach oben geschoben
                panelHaken.Top -= schrittweite;
                panelAusleger.Top -= schrittweite;
                panelMast.Height += schrittweite;
                panelMast.Top -= schrittweite;

                //Size size = panelMast.Size;
                //size.Height -= schrittweite;
                //panelBasis.Top -= 10;
            }
        }

        // Kran bzw. Mast fährt runter ...
        private void buttonMoveDown_Click(object sender, EventArgs e)
        {
            // ... bis zum Fundament bzw. Basis. 
            //            if (panelHaken.Bottom + schrittweite <= panelBasis.Top)
            if (((panelHaken.Bottom + schrittweite) <= panelBasis.Bottom)
                && ((panelAusleger.Bottom + schrittweite) <= panelBasis.Top))
            {
                // Haken und Ausleger fahren hoch 
                panelHaken.Top += schrittweite;
                panelAusleger.Top += schrittweite;
                panelMast.Height -= schrittweite;
                panelMast.Top += schrittweite;

                // panelMast.Top += 10;
                //panelBasis.Top += 10;
            }
        }
        // Haken fährt raus 
        private void buttonHakenRaus_Click(object sender, EventArgs e)
        {
            // ... solange, bis er die Basis erreicht
            if (panelHaken.Top + panelHaken.Height <= panelBasis.Top)
            {
                // Haken fährt raus
                panelHaken.Height += schrittweite;
            }
        }

        // Haken fährt rein ...
        private void buttonHakenRein_Click(object sender, EventArgs e)
        {
            // ... solange bis er den Ausleger erreicht
            if (panelHaken.Bottom >= (panelAusleger.Bottom + schrittweite))
            {
                // Haken fährt rein
                panelHaken.Height -= schrittweite;
            }
        }
        // Haken fährt nach rechts ...
        private void hakenRechts_Click(object sender, EventArgs e)
        {
            // ... bis zum Mast            
            if ((panelHaken.Right <= panelMast.Left - schrittweite)               
                && (panelHaken.Right <= panelBasis.Left - schrittweite))
            {
                panelHaken.Left += schrittweite;
            }
        }

        private void hakenLinks_Click(object sender, EventArgs e)
        {
            if(panelHaken.Left >= panelAusleger.Left + schrittweite)
            {
                panelHaken.Left -= schrittweite;
            }
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        
    }
}
